# Hugo Boilerplate

* Repository: [GitLab](https://gitlab.com)
* Static site generator: [Hugo](https://gohugo.io)

## Local Development

Install Docker then run `docker-compose up`

## Layouts

The template is based on small, content-agnostic partials that can be mixed and matched. The pre-built pages showcase just a few of the possible combinations. Refer to the `site/layouts/partials` folder for all available partials.

Use Hugo’s `dict` functionality to feed content into partials and avoid repeating yourself and creating discrepancies.

## CSS

* Utility-first framework: [TailwindCSS] (https://tailwindcss.com/)
