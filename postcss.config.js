const purgecss = require('@fullhuman/postcss-purgecss')({
    // Paths to all of the template files
    content: [
        './layouts/**/*.html',
    ],
    defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
});

const cssnano = require('cssnano')({
    preset: 'default',
});

module.exports = {
    plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
        ...process.env.NODE_ENV === 'production'
            ? [purgecss, cssnano]
            : [],
    ]
}
