document.documentElement.classList.replace('no-js','js');


function domLoaded() {

}

/* Wait for DOMContentLoaded */
if (document.readyState === 'loading') {  // Loading hasn't finished yet
    document.addEventListener('DOMContentLoaded', domLoaded);
} else {  // `DOMContentLoaded` has already fired
    domLoaded();
}
